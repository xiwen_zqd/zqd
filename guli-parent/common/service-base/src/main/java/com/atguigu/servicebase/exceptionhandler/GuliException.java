package com.atguigu.servicebase.exceptionhandler;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName GuliException
 * @Description 自定义异常类
 * @Author zqd
 * @Date 2020/3/26 13:41
 */
@Data
@AllArgsConstructor //生成有参数构造方法
@NoArgsConstructor //生成无参数构造方法
public class GuliException extends RuntimeException {

    @ApiModelProperty(value = "状态码")
    private Integer code;
    @ApiModelProperty(value = "异常信息")
    private String msg;
}
