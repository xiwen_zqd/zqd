package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
