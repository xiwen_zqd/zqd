package com.atguigu.serviceedu.entity.chapter;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName VideoVo
 * @Description TODO
 * @Author zqd
 * @Date 2020/4/3 15:04
 */
@Data
public class VideoVo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
}
