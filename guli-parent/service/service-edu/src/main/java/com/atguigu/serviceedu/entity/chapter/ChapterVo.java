package com.atguigu.serviceedu.entity.chapter;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ChapterVo
 * @Description 章节
 * @Author zqd
 * @Date 2020/4/3 14:22
 */
@Data
public class ChapterVo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    //小节
    List<VideoVo> children = new ArrayList<>();
}
