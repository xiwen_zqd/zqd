package com.atguigu.serviceedu.service.impl;

import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.atguigu.serviceedu.entity.EduChapter;
import com.atguigu.serviceedu.entity.EduVideo;
import com.atguigu.serviceedu.entity.chapter.ChapterVo;
import com.atguigu.serviceedu.entity.chapter.VideoVo;
import com.atguigu.serviceedu.mapper.EduChapterMapper;
import com.atguigu.serviceedu.service.EduChapterService;
import com.atguigu.serviceedu.service.EduVideoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    EduVideoService videoService;

    @Override
    public List<ChapterVo> findChapterVideo(String courseId) {
        //根据课程查询所有的章节
        QueryWrapper<EduChapter> eduChapterQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id",courseId);

        List<EduChapter> chapterList = baseMapper.selectList(eduChapterQueryWrapper);

        QueryWrapper<EduVideo> eduVideoQueryWrapper = new QueryWrapper<>();
        eduChapterQueryWrapper.eq("course_id",courseId);
        List<EduVideo> videoList = videoService.list(eduVideoQueryWrapper);

        List<ChapterVo> chapterVoList = new ArrayList<>();

        for (EduChapter c : chapterList) {
            ChapterVo cv = new ChapterVo();

            BeanUtils.copyProperties(c,cv);
            chapterVoList.add(cv);

            List<VideoVo> videoVoList = new ArrayList<>();
            for (EduVideo v : videoList) {
                if (v.getChapterId().equals(c.getId())) {

                    VideoVo vo = new VideoVo();
                    BeanUtils.copyProperties(v,vo);

                    videoVoList.add(vo);
                }
            }
            cv.setChildren(videoVoList);
        }

        return chapterVoList;
    }
}
