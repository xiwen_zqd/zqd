package com.atguigu.serviceedu.service;

import com.atguigu.serviceedu.entity.EduCourse;
import com.atguigu.serviceedu.entity.vo.CourseInfoVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
public interface EduCourseService extends IService<EduCourse> {
    /**
     *  保存课程信息
     * @param courseInfoVo
     * @return
     */
    String saveCourseInfo(CourseInfoVo courseInfoVo);

    /**
     * 根据Id查询课程信息
     * @param id
     * @return
     */
    CourseInfoVo findByIdCourse(String id);
}
