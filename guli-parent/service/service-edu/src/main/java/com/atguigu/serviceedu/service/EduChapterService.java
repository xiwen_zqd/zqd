package com.atguigu.serviceedu.service;

import com.atguigu.serviceedu.entity.EduChapter;
import com.atguigu.serviceedu.entity.chapter.ChapterVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
public interface EduChapterService extends IService<EduChapter> {

    List<ChapterVo> findChapterVideo(String courseId);
}
