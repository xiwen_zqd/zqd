package com.atguigu.serviceedu.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程简介 前端控制器
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@CrossOrigin
@RestController
@RequestMapping("/eduservice/courseDescription")
public class EduCourseDescriptionController {

}

