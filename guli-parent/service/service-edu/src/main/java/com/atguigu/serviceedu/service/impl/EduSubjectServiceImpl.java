package com.atguigu.serviceedu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.serviceedu.entity.EduSubject;
import com.atguigu.serviceedu.entity.excel.SubjectData;
import com.atguigu.serviceedu.entity.subject.OneSubject;
import com.atguigu.serviceedu.entity.subject.TwoSubject;
import com.atguigu.serviceedu.listener.SubjectExcelListener;
import com.atguigu.serviceedu.mapper.EduSubjectMapper;
import com.atguigu.serviceedu.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Autowired
    EduSubjectMapper subjectMapper;

    /**
     * 添加课程分类
     */
    @Override
    public void saveSubject(MultipartFile file,EduSubjectService subjectService) {

        try {
            //文件的输入流
            InputStream in = file.getInputStream();
            //调用方法进行读取
            EasyExcel.read(in, SubjectData.class,new SubjectExcelListener(subjectService)).sheet().doRead();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {

        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        wrapperOne.eq("parent_id","0");

        List<EduSubject> oneSubjectList = subjectMapper.selectList(wrapperOne);


        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperTwo.ne("parent_id","0");

        List<EduSubject> twoSubjectList = subjectMapper.selectList(wrapperTwo);

        List<OneSubject> subjectArrayList = new ArrayList<>();

        int oneLen = oneSubjectList.size();
        for (int i=0;i < oneLen ;i++) {

            OneSubject oneSubject = new OneSubject();
            BeanUtils.copyProperties(oneSubjectList.get(i),oneSubject);

            subjectArrayList.add(oneSubject);
            int twoLen = twoSubjectList.size();
            List<TwoSubject> twoSubjects = new ArrayList<>();
            for (int j=0; j< twoLen;j++) {

                if (oneSubjectList.get(i).getId().equals(twoSubjectList.get(j).getParentId())) {

                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(twoSubjectList.get(j),twoSubject);

                    twoSubjects.add(twoSubject);
                }
            }
            oneSubject.setChildren(twoSubjects);
        }

        return subjectArrayList;
    }
}
