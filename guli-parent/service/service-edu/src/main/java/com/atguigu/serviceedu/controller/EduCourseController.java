package com.atguigu.serviceedu.controller;


import com.atguigu.commonutils.R;
import com.atguigu.serviceedu.entity.vo.CourseInfoVo;
import com.atguigu.serviceedu.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@CrossOrigin
@RestController
@RequestMapping("/eduservice/course")
public class EduCourseController {

    @Autowired
    EduCourseService courseService;

    /**
     * 添加课程信息
     * @return
     */
    @PostMapping("/addCourseInfo")
    public R addCourseInfo(@RequestBody CourseInfoVo courseInfoVo){
        //返回添加之后课程id，为后面添加大纲使用
        String id = courseService.saveCourseInfo(courseInfoVo);

        return R.ok().data("courseId",id);
    }

    /**
     * 根据ID查询课程信息
     * @param id
     * @return
     */
    @GetMapping("/findByIdCourse/{id}")
    public R findByIdCourse(@PathVariable String id){
        CourseInfoVo courseInfoVo = courseService.findByIdCourse(id);
        return R.ok().data("courseInfo",courseInfoVo);
    }

}

