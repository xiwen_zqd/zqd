package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduTeacher;
import com.atguigu.serviceedu.entity.vo.TeacherQuery;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author atguigu
 * @since 2020-03-25
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
