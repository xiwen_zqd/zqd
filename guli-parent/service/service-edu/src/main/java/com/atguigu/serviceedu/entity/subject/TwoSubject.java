package com.atguigu.serviceedu.entity.subject;

import lombok.Data;

/**
 * @ClassName TwoSubject
 * @Description 二级分类
 * @Author zqd
 * @Date 2020/4/2 15:33
 */
@Data
public class TwoSubject {

    private String id;
    private String title;
}
