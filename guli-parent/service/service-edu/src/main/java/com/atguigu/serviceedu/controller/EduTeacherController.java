package com.atguigu.serviceedu.controller;


import com.atguigu.commonutils.R;
import com.atguigu.serviceedu.entity.EduTeacher;
import com.atguigu.serviceedu.entity.vo.TeacherQuery;
import com.atguigu.serviceedu.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2020-03-25
 */
@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService teacherService;


    /**
     * 查询所有
     * @return
     */
    @ApiOperation(value = "所有讲师列表")
    @GetMapping("/findAll")
    public R findAllTeacher() {

        List<EduTeacher> list = teacherService.list(null);

        return R.ok().data("items",list);
    }

    @ApiOperation(value = "根据ID删除讲师")
    @DeleteMapping("delById/{id}")
    public R removeById(
            @ApiParam(name = "id", value = "讲师ID", required = true)
            @PathVariable String id) {
        boolean flag = teacherService.removeById(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }
    @ApiOperation("分页查询")
    @GetMapping("/pageTeacher/{current}/{limit}")
    public R pageTeracherList(
            @ApiParam(name = "current", value = "当前页码", required = true)
            @PathVariable Long current,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit) {

        Page<EduTeacher> pageParam = new Page<>(current,limit);

        teacherService.page(pageParam,null);

        List<EduTeacher> records = pageParam.getRecords();

        long total = pageParam.getTotal();

        return R.ok().data("total",total).data("rows",records);
    }

    @ApiOperation("条件分页查询")
    @PostMapping("/pageTeracherCondition/{current}/{limit}")
    public R pageTeracherCondition(
            @ApiParam(name = "current", value = "当前页码", required = true)
            @PathVariable Long current,
            @ApiParam(name = "limit", value = "每页记录数", required = true)
            @PathVariable Long limit,
            @RequestBody(required = false) TeacherQuery teacherQuery) {

        Page<EduTeacher> pageParam = new Page<>(current,limit);

        teacherService.pageQuery(pageParam,teacherQuery);

        List<EduTeacher> records = pageParam.getRecords();

        long total = pageParam.getTotal();

        return R.ok().data("total",total).data("rows",records);
    }

    @ApiOperation("新增讲师")
    @PostMapping("/addTeacher")
    public R save(@RequestBody(required = true) EduTeacher eduTeacher){

        boolean flag = teacherService.save(eduTeacher);
        if (flag) {
           return R.ok();
        } else {
           return R.error();
        }
    }
    @ApiOperation(value = "根据ID查询讲师信息")
    @GetMapping("/findByIdTeacher/{id}")
    public R queryById(
            @ApiParam(name = "id",value = "讲师ID",required = true)
            @PathVariable(required = true) String id) {
        EduTeacher eduTeacher = teacherService.getById(id);
        if (eduTeacher != null) {
            return R.ok().data("items",eduTeacher);
        } else {
            return R.error();
        }
    }
    @ApiOperation(value = "修改讲师信息")
    @PutMapping("/updateTeacher/{id}")
    public R updateTeacher(
            @ApiParam(name = "id", value = "讲师ID",required = true)
            @PathVariable String id,
            @ApiParam(name = "teacher", value = "讲师对象",required = true)
            @RequestBody EduTeacher teacher) {
        teacher.setId(id);
        boolean flag = teacherService.updateById(teacher);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }


}

