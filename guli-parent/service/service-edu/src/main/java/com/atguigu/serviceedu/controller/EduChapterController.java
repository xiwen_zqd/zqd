package com.atguigu.serviceedu.controller;


import com.atguigu.commonutils.R;
import com.atguigu.serviceedu.entity.chapter.ChapterVo;
import com.atguigu.serviceedu.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@CrossOrigin
@RestController
@RequestMapping("/eduservice/chapter")
public class EduChapterController {

    @Autowired
    EduChapterService chapterService;

    @GetMapping("/getChapterVideo/{courseId}")
    public R findChapterVoList(@PathVariable String courseId){

        List<ChapterVo> chapterVoList = chapterService.findChapterVideo(courseId);

        return R.ok().data("allChapterVideo",chapterVoList);
    }

}

