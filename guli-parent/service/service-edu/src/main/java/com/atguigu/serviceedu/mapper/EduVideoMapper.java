package com.atguigu.serviceedu.mapper;

import com.atguigu.serviceedu.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
