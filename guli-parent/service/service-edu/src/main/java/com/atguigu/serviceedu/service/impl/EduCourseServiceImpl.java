package com.atguigu.serviceedu.service.impl;

import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.atguigu.serviceedu.entity.EduCourse;
import com.atguigu.serviceedu.entity.EduCourseDescription;
import com.atguigu.serviceedu.entity.vo.CourseInfoVo;
import com.atguigu.serviceedu.mapper.EduCourseMapper;
import com.atguigu.serviceedu.service.EduCourseDescriptionService;
import com.atguigu.serviceedu.service.EduCourseService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author zqd
 * @since 2020-04-02
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {
    /**
     * 课程描述的注入
     */
    @Autowired
    EduCourseDescriptionService eduCourseDescriptionService;

    @Autowired
    EduCourseMapper eduCourseMapper;
    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {
        //1、向课程表添加课程基本信息
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int count = baseMapper.insert(eduCourse);

        if (count <= 0) {
            //添加失败
            throw new GuliException(2001,"添加课程失败");
        }
        String cId = eduCourse.getId();

        //2、向课程简介表添加课程简介
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setId(cId);
        courseDescription.setDescription(courseInfoVo.getDescription());

        eduCourseDescriptionService.save(courseDescription);

        return cId;
    }

    @Override
    public CourseInfoVo findByIdCourse(String id) {

        EduCourse eduCourse = this.getById(id);
        if (eduCourse == null){
            throw new GuliException(20001,"Course数据不存在");
        }
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse,courseInfoVo);

        EduCourseDescription eduCourseDescription = eduCourseDescriptionService.getById(id);
        if (eduCourseDescription == null){
            throw new GuliException(20001,"CourseDescription数据不存在");
        }

        courseInfoVo.setDescription(eduCourseDescription.getDescription());

        return courseInfoVo;
    }
}
