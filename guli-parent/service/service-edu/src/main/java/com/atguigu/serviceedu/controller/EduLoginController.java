package com.atguigu.serviceedu.controller;

import com.atguigu.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName EduLoginController
 * @Description 登录
 * @Author zqd
 * @Date 2020/3/30 13:47
 */
@RestController
@CrossOrigin
@RequestMapping("/eduservice/user")
public class EduLoginController {
    /**
     * 登录
     * @return
     */
    @PostMapping("/login")
    public R login() {
        return R.ok().data("token","admin");
    }

    /**
     * 权限信息
     * @return
     */
    @GetMapping("/info")
    public R info() {
        return R.ok().data("roles","[admin]").data("name","admin");
    }
}
