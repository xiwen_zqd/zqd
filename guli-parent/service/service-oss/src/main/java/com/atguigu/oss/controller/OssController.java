package com.atguigu.oss.controller;

import com.atguigu.commonutils.R;
import com.atguigu.oss.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName OssController
 * @Description TODO
 * @Author zqd
 * @Date 2020/4/1 13:59
 */
@Api(description = "图片文件上传")
@CrossOrigin
@RestController
@RequestMapping("/eduoss/fileoss")
public class OssController {

    @Autowired
    FileService fileService;

    /**
     * 头像上传
     * @return
     */
    @ApiOperation(value = "图片or文件上传")
    @PostMapping
    public R uploadOssFile(MultipartFile file){
        //获取文件上传 MultipartFile
        //返回上传Oss的路径
        String url = fileService.upload(file);

        return R.ok().data("url",url);
    }
}
