import request from '@/utils/request'

export default {

    //1 讲师列表（条件分页查询）
    //current当前页  limit每页记录数 teacherQuery条件对象
    getTeracherListPage(current,limit,teacherQuery){
        return request({
            //url: '/eduservice/teacher/pageTeracherCondition/'+current+'/'+limit,
            url: `/eduservice/teacher/pageTeracherCondition/${current}/${limit}`,
            method: 'post',
            //data表示把对象转换json进行传递到接口里面
            data: teacherQuery
          })
    },
    //删除
    removeById(teacherId) {
        return request({
            url: `/eduservice/teacher/delById/${teacherId}`,
            method: 'delete'
        })
    },
    //添加讲师
    addTeacher(teacher){
        return request({
            url: `/eduservice/teacher/addTeacher`,
            method: 'post',
            data: teacher
        })
    },
    //教师信息回显
    getTeacherById(id) {
        return request({
            url: `/eduservice/teacher/findByIdTeacher/${id}`,
            method: 'get'
        })
    },
    //修改讲师信息
    updateTeacher(teacher) {
        return request({
            url: `/eduservice/teacher/updateTeacher/${teacher.id}`,
            method: 'put',
            data: teacher
        })
    },
    //查询所有讲师
    findAllTeacher(){
        return request({
            url: `/eduservice/teacher/findAll`,
            method: 'get'
        })
    }
}