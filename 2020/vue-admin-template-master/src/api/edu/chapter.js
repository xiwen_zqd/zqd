import request from '@/utils/request'

export default{
    
    //根据id查询章节和小节数据列表
    findCourseById(courseId){
        return request({
            url: `/eduservice/course/findByIdCourse/${courseId}`,
            method: 'get'
          }) 
    }
}